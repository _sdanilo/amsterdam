﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Amsterdan
{
    public partial class Form2 : Form
    {
        #region Construtor, Form_Load e atributos padrões

        SolidBrush branco = new SolidBrush(Color.GhostWhite);
        SolidBrush preto = new SolidBrush(Color.Black);
        SolidBrush vermelho = new SolidBrush(Color.Red);
        SolidBrush verde = new SolidBrush(Color.Lime);
        SolidBrush defaultColor = new SolidBrush(Color.SlateGray);
        Font font = new Font("Arial", 10.0f);
        private bool inicializacao;
        Arvore arvore = new Arvore();
        List<int> listaInsercao = new List<int>();

        public Form2(bool ini)
        {
            inicializacao = ini;
            InitializeComponent();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            if (inicializacao)
                rbtArvoreBinaria.Checked = true;
            if (!inicializacao)
                rbtBubbleSort.Checked = true;
            DialogResult preLoad = MessageBox.Show("Deseja carregar a última estrutura de dados salva?", "questão", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (preLoad == DialogResult.Yes)
            {
                this.Focus();
                Serializa serializa = new Serializa();
                List<int> listaNodes = serializa.carrega();
                for (int i = 0; i < listaNodes.Count; i++)
                {
                    inserir(listaNodes[i], false);
                }
                MessageBox.Show("Lista carregada com sucesso!");
            }
        }
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Form1 f = new Form1();
            //f.Visible = true;
        }
        #endregion

        #region Metodos Graficos

        public void iluminaPercurso(No node)
        {
            List<No> nodeList = new List<No>();
            while (node != null)
            {
                nodeList.Add(node);
                node = node.pai;
            }
            No temp;
            ///
            /// Iluminando o percurso até a insersão/seleção do Node
            /// que será iluminado por último.
            /// 
            for (int i = 0; i < nodeList.Count; i++)
            {
                temp = nodeList[nodeList.Count - i - 1];
                iluminaNode(temp.x, temp.y, temp.dado, vermelho);
            }
            //Iluminando o node inserido/selecionado
            iluminaNode(nodeList[0].x, nodeList[0].y, nodeList[0].dado, verde);
        }

        public void iluminaNode(int x, int y, int valor, SolidBrush cor)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(x - 3, y - 3, 30, 26);
            Rectangle bordaRetangulo = new Rectangle(x - 4, y - 4, 32, 28);
            SolidBrush corBordaRetangulo = preto;
            for (int i = 0; i < 4; i++)
            {
                Thread.Sleep(100);
                if (corBordaRetangulo == preto)
                    corBordaRetangulo = cor;
                else
                    corBordaRetangulo = preto;
                formGraphics.FillEllipse(corBordaRetangulo, bordaRetangulo);
                formGraphics.FillEllipse(branco, retangulo);
                formGraphics.DrawString(valor.ToString(), font, preto, x, y);
            }
            Thread.Sleep(200);
        }

        public void iluminaPercursoSimples(No node)
        {
            List<No> nodeList = new List<No>();
            while (node != null)
            {
                nodeList.Add(node);
                node = node.pai;
            }
            No temp;
            ///
            /// Iluminando o percurso até a insersão/seleção do Node
            /// que será iluminado por último.
            /// 
            for (int i = 0; i < nodeList.Count; i++)
            {
                temp = nodeList[nodeList.Count - i - 1];
                iluminaNodeSimples(temp.x, temp.y, temp.dado);
            }
            //Iluminando o node inserido/selecionado
            iluminaNode(nodeList[0].x, nodeList[0].y, nodeList[0].dado, verde);
        }

        public void iluminaNodeSimples(int x, int y, int valor)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(x - 3, y - 3, 30, 26);
            Rectangle bordaRetangulo = new Rectangle(x - 4, y - 4, 32, 28);
            SolidBrush corBordaRetangulo = preto;
            corBordaRetangulo = preto;
            formGraphics.FillEllipse(corBordaRetangulo, bordaRetangulo);
            formGraphics.FillEllipse(branco, retangulo);
            formGraphics.DrawString(valor.ToString(), font, preto, x, y);
        }
        #endregion

        //Metodo iniciado ao confirmar o valor inserido
        public void executarAcao()
        {
            if (rbtArvoreBinaria.Checked)
            {
                if (rbtInserir.Checked)
                {
                    try
                    {
                        inserir(Convert.ToInt32(txtboxAction.Text), true);
                        txtboxAction.Text = String.Empty;
                    }
                    catch (Exception erro)
                    {
                        MessageBox.Show("Preencha corretamente o campo com o valor à ser inserido\r\n" + erro.Message, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    txtboxAction.Focus();
                }
                if (rbtSelecionar.Checked)
                {
                    try
                    {
                        selecionar(Convert.ToInt32(txtboxAction.Text));
                        txtboxAction.Text = String.Empty;
                    }
                    catch (Exception erro)
                    {
                        MessageBox.Show("Preencha corretamente o campo com o valor à ser inserido\r\n" + erro.Message, "Alerta");
                    }
                    txtboxAction.Focus();
                }
                if (rbtRemover.Checked)
                {
                    try
                    {
                        remover(Convert.ToInt32(txtboxAction.Text));
                        txtboxAction.Text = String.Empty;
                    }
                    catch (Exception erro)
                    {
                        MessageBox.Show("Preencha corretamente o campo com o valor à ser inserido\r\n" + erro.Message, "Alerta");
                    }
                    txtboxAction.Focus();
                }
            }
        }

        public void inserir(int valor, bool tipoInsercao)
        {
            if (arvore == null)
                arvore = new Arvore();
            No node = new No();
            SerializaXml save = new SerializaXml();
            node = arvore.insere(Convert.ToInt32(valor));
            if (node == null)
            {
                lblAlerta.Text = "O valor " + valor + " já foi inserido anteriormente";
            }
            else
            {
                listaInsercao.Add(node.dado);
                if (tipoInsercao)
                    iluminaPercurso(node);
                else
                    iluminaPercursoSimples(node);
                richTextBox1.Text += node.hierarquia + "\r\n";
            }
            Application.DoEvents();
        }

        public void selecionar(int valor)
        {
            if (arvore == null)
                arvore = new Arvore();
            No node = new No();
            node = arvore.seleciona(Convert.ToInt32(valor));
            if (node == null)
            {
                lblAlerta.Text = "O valor " + valor + " não existe na Arvore";
            }
            else
            {
                iluminaPercurso(node);
                richTextBox1.Text += node.hierarquia + "\r\n";
            }
        }

        public void remover(int valor)
        {
            No node = new No();
            node = arvore.seleciona(Convert.ToInt32(valor));
            arvore.remove(valor);
            iluminaNode(node.x, node.y, valor,defaultColor);
        }

        #region Capturando o valor para inserção

        private void btnAction_Click(object sender, EventArgs e)
        {
            executarAcao();
        }

        private void txtboxAction_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                executarAcao();
        }

        #endregion

        #region Inserção automática de diversos valores, para teste.

        private void btnTeste_Click(object sender, EventArgs e)
        {
            List<int> x = new List<int>();
            x.Add(200);
            x.Add(100);
            x.Add(300);
            x.Add(50);
            x.Add(150);
            x.Add(250);
            x.Add(350);
            x.Add(25);
            x.Add(75);
            x.Add(125);
            x.Add(175);
            x.Add(225);
            x.Add(275);
            x.Add(325);
            x.Add(375);
            x.Add(12);
            x.Add(37);
            x.Add(62);
            x.Add(87);
            x.Add(112);
            x.Add(137);
            x.Add(162);
            x.Add(187);
            x.Add(212);
            x.Add(237);
            x.Add(262);
            x.Add(287);
            x.Add(312);
            x.Add(337);
            x.Add(362);
            x.Add(387);

            for (int i = 0; i < x.Count; i++)
            {
                inserir(x[i],false);
            }

            /*
            Random r = new Random();
            for (int i = 0; i < 5; i++)
            {
                inserir(r.Next(30, 60));
            }
            for (int i = 0; i < 10; i++)
            {
                inserir(r.Next(0, 99));
            }
            */
        }
        #endregion

        #region Limpando a tela e esvaziando a Arvore/BubbleSort

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(0, 0, panel1.Width, panel1.Height);
            formGraphics.FillRectangle(defaultColor, retangulo);
            richTextBox1.Clear();
            arvore = null;
        }
        #endregion

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Serializa serializa = new Serializa();
            serializa.salvar(listaInsercao);
            MessageBox.Show("Lista salva com sucesso!");
        }

        private void btnCarregar_Click(object sender, EventArgs e)
        {
            Serializa serializa = new Serializa();
            List<int> listaNodes = serializa.carrega();
            for (int i = 0; i < listaNodes.Count; i++)
            {
                inserir(listaNodes[i], false);
            }
            MessageBox.Show("Lista carregada com sucesso!");
        }
    }
}
