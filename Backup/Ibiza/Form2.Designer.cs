﻿namespace Amsterdan
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLimpar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnTeste = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.rbtSelecionar = new System.Windows.Forms.RadioButton();
            this.rbtRemover = new System.Windows.Forms.RadioButton();
            this.rbtInserir = new System.Windows.Forms.RadioButton();
            this.txtboxAction = new System.Windows.Forms.TextBox();
            this.rbtArvoreBinaria = new System.Windows.Forms.RadioButton();
            this.rbtBubbleSort = new System.Windows.Forms.RadioButton();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAlerta = new System.Windows.Forms.Label();
            this.btnCarregar = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(9, 160);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(98, 40);
            this.btnLimpar.TabIndex = 9;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCarregar);
            this.groupBox2.Controls.Add(this.btnTeste);
            this.groupBox2.Controls.Add(this.btnAction);
            this.groupBox2.Controls.Add(this.btnSalvar);
            this.groupBox2.Controls.Add(this.rbtSelecionar);
            this.groupBox2.Controls.Add(this.rbtRemover);
            this.groupBox2.Controls.Add(this.btnLimpar);
            this.groupBox2.Controls.Add(this.rbtInserir);
            this.groupBox2.Controls.Add(this.txtboxAction);
            this.groupBox2.Location = new System.Drawing.Point(2, 103);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 348);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ações";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(10, 252);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(97, 40);
            this.btnSalvar.TabIndex = 16;
            this.btnSalvar.Text = "Salvar lista de Inserção";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnTeste
            // 
            this.btnTeste.Location = new System.Drawing.Point(10, 206);
            this.btnTeste.Name = "btnTeste";
            this.btnTeste.Size = new System.Drawing.Size(97, 40);
            this.btnTeste.TabIndex = 15;
            this.btnTeste.Text = "Simulação Automática";
            this.btnTeste.UseVisualStyleBackColor = true;
            this.btnTeste.Click += new System.EventHandler(this.btnTeste_Click);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(9, 114);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(100, 40);
            this.btnAction.TabIndex = 13;
            this.btnAction.Text = "Ok";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // rbtSelecionar
            // 
            this.rbtSelecionar.AutoSize = true;
            this.rbtSelecionar.Location = new System.Drawing.Point(6, 65);
            this.rbtSelecionar.Name = "rbtSelecionar";
            this.rbtSelecionar.Size = new System.Drawing.Size(75, 17);
            this.rbtSelecionar.TabIndex = 14;
            this.rbtSelecionar.Text = "Selecionar";
            this.rbtSelecionar.UseVisualStyleBackColor = true;
            // 
            // rbtRemover
            // 
            this.rbtRemover.AutoSize = true;
            this.rbtRemover.Location = new System.Drawing.Point(6, 42);
            this.rbtRemover.Name = "rbtRemover";
            this.rbtRemover.Size = new System.Drawing.Size(68, 17);
            this.rbtRemover.TabIndex = 13;
            this.rbtRemover.Text = "Remover";
            this.rbtRemover.UseVisualStyleBackColor = true;
            // 
            // rbtInserir
            // 
            this.rbtInserir.AutoSize = true;
            this.rbtInserir.Checked = true;
            this.rbtInserir.Location = new System.Drawing.Point(6, 19);
            this.rbtInserir.Name = "rbtInserir";
            this.rbtInserir.Size = new System.Drawing.Size(53, 17);
            this.rbtInserir.TabIndex = 12;
            this.rbtInserir.TabStop = true;
            this.rbtInserir.Text = "Inserir";
            this.rbtInserir.UseVisualStyleBackColor = true;
            // 
            // txtboxAction
            // 
            this.txtboxAction.Location = new System.Drawing.Point(9, 88);
            this.txtboxAction.Name = "txtboxAction";
            this.txtboxAction.Size = new System.Drawing.Size(100, 20);
            this.txtboxAction.TabIndex = 3;
            this.txtboxAction.Text = "20";
            this.txtboxAction.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtboxAction.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtboxAction_KeyDown);
            // 
            // rbtArvoreBinaria
            // 
            this.rbtArvoreBinaria.AutoSize = true;
            this.rbtArvoreBinaria.Location = new System.Drawing.Point(12, 31);
            this.rbtArvoreBinaria.Name = "rbtArvoreBinaria";
            this.rbtArvoreBinaria.Size = new System.Drawing.Size(91, 17);
            this.rbtArvoreBinaria.TabIndex = 11;
            this.rbtArvoreBinaria.TabStop = true;
            this.rbtArvoreBinaria.Text = "Arvore Binária";
            this.rbtArvoreBinaria.UseVisualStyleBackColor = true;
            // 
            // rbtBubbleSort
            // 
            this.rbtBubbleSort.AutoSize = true;
            this.rbtBubbleSort.Location = new System.Drawing.Point(12, 54);
            this.rbtBubbleSort.Name = "rbtBubbleSort";
            this.rbtBubbleSort.Size = new System.Drawing.Size(80, 17);
            this.rbtBubbleSort.TabIndex = 12;
            this.rbtBubbleSort.TabStop = true;
            this.rbtBubbleSort.Text = "Bubble Sort";
            this.rbtBubbleSort.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Info;
            this.richTextBox1.Location = new System.Drawing.Point(123, 352);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(469, 105);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(123, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(469, 316);
            this.panel1.TabIndex = 13;
            // 
            // lblAlerta
            // 
            this.lblAlerta.AutoSize = true;
            this.lblAlerta.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlerta.Location = new System.Drawing.Point(12, 9);
            this.lblAlerta.Name = "lblAlerta";
            this.lblAlerta.Size = new System.Drawing.Size(17, 19);
            this.lblAlerta.TabIndex = 14;
            this.lblAlerta.Text = "-";
            // 
            // btnCarregar
            // 
            this.btnCarregar.Location = new System.Drawing.Point(10, 296);
            this.btnCarregar.Name = "btnCarregar";
            this.btnCarregar.Size = new System.Drawing.Size(97, 40);
            this.btnCarregar.TabIndex = 17;
            this.btnCarregar.Text = "Carregar último teste";
            this.btnCarregar.UseVisualStyleBackColor = true;
            this.btnCarregar.Click += new System.EventHandler(this.btnCarregar_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 463);
            this.Controls.Add(this.lblAlerta);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.rbtBubbleSort);
            this.Controls.Add(this.rbtArvoreBinaria);
            this.Controls.Add(this.groupBox2);
            this.MaximumSize = new System.Drawing.Size(612, 490);
            this.MinimumSize = new System.Drawing.Size(612, 490);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtboxAction;
        private System.Windows.Forms.RadioButton rbtArvoreBinaria;
        private System.Windows.Forms.RadioButton rbtBubbleSort;
        private System.Windows.Forms.RadioButton rbtSelecionar;
        private System.Windows.Forms.RadioButton rbtRemover;
        private System.Windows.Forms.RadioButton rbtInserir;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Button btnTeste;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblAlerta;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCarregar;
    }
}