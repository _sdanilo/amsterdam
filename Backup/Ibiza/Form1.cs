﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Amsterdan
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            Form2 f = new Form2(true);
            f.Show();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2(false);
            f.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
