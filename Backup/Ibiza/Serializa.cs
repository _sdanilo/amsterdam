﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amsterdan
{
    class Serializa : ISerializa
    {
        #region ISerializa Members

        public void salvar(List<int> listaInsercao)
        {
            SerializaXml serializa = new SerializaXml();
            try
            {
                serializa.salvar(listaInsercao);
            }
            catch (Exception erro)
            {
                throw new Exception("Falha ao salvar os dados", erro);
            } 
        }

        public List<int> carrega()
        {
            SerializaXml serializa = new SerializaXml();
            List<int> listaDeNodes;
            try
            {
                listaDeNodes = serializa.carrega();
            }
            catch (Exception erro)
            {
                throw new Exception("Falha ao carregar os dados", erro);
            }
            return listaDeNodes;
        }

        #endregion
    }
}
