﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amsterdan
{
    class Arvore
    {
        No raiz;

        #region Inserção
        public No insere(int v)
        {
            return insereI(v);
        }

        No insereI(int v)
        {
            No p, q, novo;

            novo = new No(v);

            q = null;
            p = raiz;

            int yPosition = 0;

            ///
            /// Conforme percorremos a arvore, 
            /// somamos um valor em Y (height) fazendo o node descer.
            /// 
            while (p != null)
            {
                q = p;
                if (v < p.dado) // Percorre à arvore para a esquerda
                {
                    yPosition += 30;
                    p = p.esq;
                }
                else
                    if (v > p.dado) // Percorre à arvore para a direita
                    {
                        yPosition += 30;
                        p = p.dir;
                    }
                    else
                        if (v == p.dado)
                            return null; // Caso o valor inserido já exista na árvore, retorna null
            }


            if (raiz == null)
            {
                raiz = novo; // Se a raiz for nula, define a raiz
                novo.hierarquia += novo.dado + " Raiz da àrvore.";
            }

            else // Se a raiz já houver sido definida
            {
                ///
                /// Caso o novo Node possua avô (pai.pai), o calculo da posição do mesmo
                /// precisa ser realizado com base na posição do pai e avô.
                /// 
                /// Se para a direita, pai + ((diferença entre avô e pai) / 2)
                /// ou = + ((novo.pai.pai.x - novo.pai.x) / 2)
                /// Se para a esquerda, pai - ((diferença entre avô e pai) / 2)
                /// ou = - ((novo.pai.pai.x - novo.pai.x) / 2)
                /// 
                /// Obs.:
                /// Há a necessidade de tornar a diferença sempre positiva.
                /// 

                int temp;
                if (v < q.dado)
                {
                    q.esq = novo;
                    novo.pai = q;
                    if (novo.pai.pai != null)
                    {
                        temp = ((novo.pai.pai.x - novo.pai.x) / 2);
                        if (temp < 0)
                            temp = temp * (-1);
                        novo.x = novo.pai.x - temp;
                        novo.hierarquia = novo.dado + " - filho esquerdo de: " + novo.pai.hierarquia;
                    }
                    else if (novo.pai != null)
                    {
                        novo.x = novo.pai.x - (novo.pai.x / 2);
                        novo.hierarquia = novo.dado + " - Filho esquerdo de: " + novo.pai.dado;
                    }
                }
                else
                {
                    q.dir = novo;
                    novo.pai = q;
                    if (novo.pai.pai != null)
                    {
                        temp = ((novo.pai.pai.x - novo.pai.x) / 2);
                        if (temp < 0)
                            temp = temp * (-1);
                        novo.x = novo.pai.x + temp;
                        novo.hierarquia = novo.dado + " - Filho direito de: " + novo.pai.hierarquia;
                    }
                    else if (novo.pai != null)
                    {
                        novo.x = novo.pai.x + (novo.pai.x / 2);
                        novo.hierarquia = novo.dado + " - Filho direito de: " + novo.pai.dado;
                    }
                }
            }
            novo.y += yPosition;
            return novo;

        }
        #region Inserção básica, não utilizada.
        No insere(No p, int v)
        {
            if (p == null)
            {
                p = new No();
                p.dado = v;
                p.esq = null;
                p.dir = null;
            }
            else

                if (v < p.dado)
                    p.esq = insere(p.esq, v);
                else
                    if (v > p.dado)
                        p.dir = insere(p.dir, v);

            return p;
        }
        #endregion
        #endregion

        #region Seleção
        public No seleciona(int v)
        {
            return selecionaI(v);
        }
        No selecionaI(int v)
        {
            No p = raiz;

            ///
            /// Conforme percorremos a arvore, 
            /// somamos um valor em Y (height) fazendo o node descer.
            /// 
            while (p != null)
            {
                if (v < p.dado) // Percorre à arvore para a esquerda
                {
                    p = p.esq;
                }
                else
                    if (v > p.dado) // Percorre à arvore para a direita
                    {
                        p = p.dir;
                    }
                    else
                        if (v == p.dado)
                            return p; // Caso o valor inserido já exista na árvore, retorna null
            }
            return null;
        }

        #endregion

        public void mostrar()
        {
            //ConsoleExt.Clear();
            int x = 5, y = 0;

            inorder(raiz, ref x, y);
        }

        public void remove(int k)
        {
            raiz = remove(raiz, k);
        }

        No remove(No p, int k)
        {
            // se não encontrou volta na recursão
            if (p == null)
                return null;
            else
            {
                if (k < p.dado)
                    p.esq = remove(p.esq, k);
                else
                    if (k > p.dado)
                        p.dir = remove(p.dir, k);
                    else // se encontrou o no faz a remoção do filho a esquerda
                    {
                        // verifica se o nó tem filho a esquerda
                        if (p.esq != null)
                        {
                            // buscar o filho mais a direita da subarvore esquerda
                            No aux = maisDireita(p.esq);
                            p.dado = aux.dado;

                            // remover o filho mais a direita da subarvore esquerda
                            p.esq = remove(p.esq, aux.dado);
                        }
                        else
                            if (p.dir != null)
                            {
                                // buscar o filho mais a esquerda da subarvore direita
                                No aux = maisEsquerda(p.dir);
                                p.dado = aux.dado;

                                // remover o filho mais a esquerda da subarvore direita
                                p.dir = remove(p.dir, aux.dado);
                            }
                            else p = null;

                    }
            }
            return p;
        }

        No maisDireita(No p)
        {
            while (p.dir != null)
                p = p.dir;

            return p;
        }

        No maisEsquerda(No p)
        {
            while (p.esq != null)
                p = p.esq;

            return p;
        }

        void preorder(No p)
        {
            if (p != null)
            {
                Console.WriteLine(p.dado);
                preorder(p.esq);
                preorder(p.dir);
            }
        }
        void inorder(No p, ref int x, int y)
        {
            if (p != null)
            {
                inorder(p.esq, ref x, y + 1);
                //ConsoleExt.gotoxy(x, y);
                Console.WriteLine(p.dado);
                x += 4;
                inorder(p.dir, ref x, y + 1);
            }
        }


    }
}
