﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Amsterdan
{
    public partial class Form3 : Form
    {

        private int posx, paux, p1, p2, p3, p4, p5;


        public Form3()
        {
            InitializeComponent();
        }
 
        public void Remover()
        {
            String valor1 = textBox1.Text;
            if (valor1 != "")
            {
                int op = Convert.ToInt32(valor1);

                if (op == p1)
                {
                    p1 = 0;

                    listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");

                    if (op == p2)
                    {

                        p2 = 0;
                        paux = p1;
                        p1 = p2;
                        p2 = paux;

                        listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");
                    }

                    if (op == p3)
                    {
                        p3 = 0;
                        paux = p1;
                        p1 = p3;
                        p3 = p2;
                        p2 = paux;

                        listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");

                    }

                    if (op == p4)
                    {

                        p4 = 0;
                        paux = p1;
                        p1 = p4;
                        p4 = p3;
                        p3 = p2;
                        p2 = paux;

                        listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");


                    }

                    if (op == p5)
                    {

                        p5 = 0;
                        paux = p1;
                        p1 = p5;
                        p5 = p4;
                        p4 = p3;
                        p3 = p2;
                        p2 = paux;

                        listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");

                    }

                }
            }

            listBox1.Items.Add(" ");
        }

        public void Incluir()
        {

            String valor1 = textBox1.Text;
            if (valor1 != "")
            {
                int posx = Convert.ToInt32(valor1);

                if (posx > p1)
                {
                    paux = p1;
                    p1 = posx;
                    posx = paux;

                    listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");
                                   
                }

                if (p1 > p2)
                {
                    paux = p2;
                    p2 = p1;
                    p1 = paux;

                    listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");

                    if (p2 > p3)
                    {
                        paux = p3;
                        p3 = p2;
                        p2 = paux;

                        listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");

                        if (p3 > p4)
                        {
                            paux = p4;
                            p4 = p3;
                            p3 = paux;

                            listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");

                            if (p4 > p5)
                            {
                                paux = p5;
                                p5 = p4;
                                p4 = paux;

                                listBox1.Items.Add(" | " + p1 + " | " + p2 + " | " + p3 + " | " + p4 + " | " + p5 + " | ");


                            }
                        }
                    }
                }

            }
            listBox1.Items.Add(" ");
        }

        private void criaBolhas(int x, int y, int valor)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            for (int i = 0; i < 4; i++)
            {
                formGraphics.FillEllipse(new SolidBrush(Color.Black), new Rectangle(x - 1 , y - 1, 42, 38));
                formGraphics.FillEllipse(new SolidBrush(Color.White), new Rectangle(x , y, 40, 36));
                formGraphics.DrawString(valor.ToString(), new Font("Arial", 10.0f), new SolidBrush(Color.Black), x+15, y+10);
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Remover();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            criaBolhas(50, 100, 5);
            
            if (rbtInserir.Checked)
                Incluir();
            if (rbtRemover.Checked)
                Remover();

        }


    }
}
