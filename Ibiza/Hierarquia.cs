﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amsterdan
{
    class Hierarquia
    {
        string retHierarquia;
        public string verHierarquia(No node)
        {
            string retHierarquia;
            if (node.pai == null)
                retHierarquia = "O elemento "+ node.dado +" se tornou a Raiz da Arvore. \r\n";
            else
                retHierarquia = "O elemento " + node.dado + " possui " + node.pai.dado + " como Pai. \r\n";

            if (node.esq == null && node.dir == null)
                retHierarquia += "O elemento " + node.dado + " não possui filhos. \r\n";
            else{
                if (node.esq != null)
                    retHierarquia += node.esq.dado + " É filho esquerdo de " + node.dado + ". ";
                if (node.dir != null)
                    retHierarquia += node.dir.dado + " É filho direito de " + node.dado + ". ";
            }
            return retHierarquia;
        }
    }
}
