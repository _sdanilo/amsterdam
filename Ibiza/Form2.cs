﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Amsterdan
{
    public partial class Form2 : Form
    {
        #region Construtor, Form_Load e atributos padrões

        Arvore arvore = new Arvore();
        Hierarquia hierarquia = new Hierarquia();

        public Form2()
        {
            InitializeComponent();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            richTextBox1.Text = "Escolha a ação a ser executada entre as opções do lado esquerdo";
        }
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
        #endregion

        #region Metodos Graficos

        SolidBrush amarelo = new SolidBrush(Color.Yellow);
        SolidBrush branco = new SolidBrush(Color.GhostWhite);
        SolidBrush preto = new SolidBrush(Color.Black);
        SolidBrush vermelho = new SolidBrush(Color.Red);
        SolidBrush verde = new SolidBrush(Color.Lime);
        SolidBrush defaultColor = new SolidBrush(Color.SlateGray);
        Font font = new Font("Arial", 10.0f);
        int threadingTime = 50;

        // Pop-up Exclamation com o texto passado como parâmetro
        void alert(string texto)
        {
            MessageBox.Show(texto, "Amsterdan - Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Desenha o percurso de inserção do node passado como parâmetro.
        // Cada node no percurso tem um Sleep para mostrar o caminho realizado
        public void iluminaPercurso(No node)
        {
            List<No> nodeList = new List<No>();
            while (node != null)
            {
                nodeList.Add(node);
                node = node.pai;
            }
            No temp;
            ///
            /// Iluminando o percurso até a insersão/seleção do Node
            /// que será iluminado por último.
            /// 
            for (int i = 0; i < nodeList.Count; i++)
            {
                temp = nodeList[nodeList.Count - i - 1];
                iluminaNode(temp.x, temp.y, temp.dado, vermelho);
                iluminaNodeY(temp.x, temp.y, temp.dado);
            }
            //Iluminando o node inserido/selecionado
            iluminaNode(nodeList[0].x, nodeList[0].y, nodeList[0].dado, verde);
            redesenhaArvore();
        }

        // Desenha o percurso do Node sem Sleep, utilizando o Sleep somente no node do parâmetro.
        // Utliza o iluminaNodeSimples e iluminaNode 
        public void iluminaPercursoSimples(No node)
        {
            List<No> nodeList = new List<No>();
            while (node != null)
            {
                nodeList.Add(node);
                node = node.pai;
            }
            No temp;
            ///
            /// Iluminando o percurso até a insersão/seleção do Node
            /// que será iluminado por último.
            /// 
            for (int i = 0; i < nodeList.Count; i++)
            {
                temp = nodeList[nodeList.Count - i - 1];
                iluminaNodeSimples(temp.x, temp.y, temp.dado);
            }
            //Iluminando o node inserido/selecionado
            iluminaNode(nodeList[0].x, nodeList[0].y, nodeList[0].dado, verde);

        }

        // Desenha um node e faz o mesmo piscar duas vezes, na cor informada como parâmetro
        public void iluminaNode(int x, int y, int valor, SolidBrush cor)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(x - 3, y - 3, 30, 26);
            Rectangle bordaRetangulo = new Rectangle(x - 4, y - 4, 32, 28);
            SolidBrush corBordaRetangulo = preto;
            for (int i = 0; i < 4; i++)
            {
                Thread.Sleep(threadingTime);
                if (corBordaRetangulo == preto)
                    corBordaRetangulo = cor;
                else
                    corBordaRetangulo = preto;
                formGraphics.FillEllipse(corBordaRetangulo, bordaRetangulo);
                formGraphics.FillEllipse(branco, retangulo);
                formGraphics.DrawString(valor.ToString(), font, preto, x, y);
            }
        }

        // Desenha um Node em amarelo. Utilizado na remoção.
        public void iluminaNodeY(int x, int y, int valor)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(x - 3, y - 3, 30, 26);
            Rectangle bordaRetangulo = new Rectangle(x - 4, y - 4, 32, 28);
            SolidBrush corBordaRetangulo = preto;
            for (int i = 0; i < 4; i++)
            {
                Thread.Sleep(threadingTime);
                corBordaRetangulo = preto;
                formGraphics.FillEllipse(corBordaRetangulo, bordaRetangulo);
                formGraphics.FillEllipse(amarelo, retangulo);
                formGraphics.DrawString(valor.ToString(), font, preto, x, y);
            }
            //Thread.Sleep(200);
        }

        // Desenha um Node sem piscar com os efeitos utilizados normamente na inserção.
        // Usado para redesenhar a arvore.
        public void iluminaNodeSimples(int x, int y, int valor)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(x - 3, y - 3, 30, 26);
            Rectangle bordaRetangulo = new Rectangle(x - 4, y - 4, 32, 28);
            SolidBrush corBordaRetangulo = preto;
            corBordaRetangulo = preto;
            formGraphics.FillEllipse(corBordaRetangulo, bordaRetangulo);
            formGraphics.FillEllipse(branco, retangulo);
            formGraphics.DrawString(valor.ToString(), font, preto, x, y);
        }

        // Desenha um circulo com defaultColor em cima dos Parametros X e Y
        public void apagaNode(int x, int y)
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(x - 4, y - 4, 32, 28);
            formGraphics.FillEllipse(defaultColor, retangulo);
        }

        // Caso o Panel precise ser redesenhado, a arvore presente no mesmo é redesenhada junto com ele
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            redesenhaArvore();
        }

        #endregion Metodos Graficos

        #region Principais Ações

        // Metodo iniciado ao confirmar o valor inserido
        public void executarAcao()
        {
            if (rbtInserir.Checked)
            {
                try
                {
                    int numInserido = Convert.ToInt32(txtboxAction.Text);
                    txtboxAction.Text = String.Empty;
                    inserir(numInserido, true);
                }
                catch (Exception erro)
                {
                    alert("Preencha corretamente o campo com o valor à ser inserido\r\n" + erro.Message);
                }
                txtboxAction.Focus();
            }
            if (rbtSelecionar.Checked)
            {
                try
                {
                    int numInserido = Convert.ToInt32(txtboxAction.Text);
                    txtboxAction.Text = String.Empty;
                    selecionar(numInserido);
                }
                catch (Exception erro)
                {
                    alert("Preencha corretamente o campo com o valor selecionado\r\n" + erro.Message);
                }
                txtboxAction.Focus();
            }
            if (rbtRemover.Checked)
            {
                try
                {
                    remover(Convert.ToInt32(txtboxAction.Text));
                    txtboxAction.Text = String.Empty;
                }
                catch (Exception erro)
                {
                    alert("Preencha corretamente o campo com o valor à ser removido\r\n" + erro.Message);
                }
                txtboxAction.Focus();
            }

            if (rbtMostrar.Checked)
            {
                try
                {
                    mostraArvore(Convert.ToInt32(txtboxAction.Text));
                    txtboxAction.Text = String.Empty;
                }
                catch (Exception erro)
                {
                    alert("Preencha corretamente o campo com o valor à ser selecionado\r\n" + erro.Message);
                }
                txtboxAction.Focus();
            }
        }

        // Execução uma inserção na arvore e faz as chamadas dos metodos graficos
        public void inserir(int valor, bool tipoInsercao)
        {
            try
            {
                if (arvore == null)
                    arvore = new Arvore();

                No node = new No();
                node = arvore.insere(Convert.ToInt32(valor));
                if (node == null)
                {
                    richTextBox1.Text = "O valor " + valor + " já foi inserido anteriormente";
                }
                else
                {
                    if (tipoInsercao)
                        iluminaPercurso(node);
                    else
                        iluminaPercursoSimples(node);

                    richTextBox1.Text = hierarquia.verHierarquia(node);
                }
                Application.DoEvents();
            }
            catch (Exception erro)
            {
                alert("Erro ocorrido na inserção.\r\n" + erro.Message);
            }
        }

        // Execução uma seleção na arvore e faz as chamadas dos metodos graficos
        public void selecionar(int valor)
        {
            if (arvore == null)
                arvore = new Arvore();
            No node = new No();
            node = arvore.seleciona(Convert.ToInt32(valor));
            if (node == null)
            {
                richTextBox1.Text = "O valor " + valor + " não existe na Arvore";
            }
            else
            {
                iluminaPercurso(node);
                richTextBox1.Text = hierarquia.verHierarquia(node);
            }
        }

        // Chama o inorder da arvore, obtendo todos seus nodes, redesenha a arvore com iluminaNodeSimples
        public void redesenhaArvore()
        {
            List<int> lista = arvore.inorder();
            No node;
            foreach (int i in lista)
            {
                node = arvore.seleciona(i);
                iluminaNodeSimples(node.x, node.y, node.dado);
            }
        }

        // Realiza um Inorder com um node selecionado, acendendo todos os seus filhos.
        // Após, redesenha a arvore, apagando os nodes pintados de amarelo.
        public void mostraArvore(int valor)
        {
            No node = arvore.seleciona(Convert.ToInt32(valor));
            if (node == null)
            {
                richTextBox1.Text = "O valor " + valor + " não existe na Arvore";
            }
            else
            {
                List<int> lista = arvore.inorder(valor);
                foreach (int i in lista)
                {
                    node = arvore.seleciona(i);
                    iluminaNode(node.x, node.y, node.dado, verde);
                    iluminaNodeY(node.x, node.y, node.dado);
                }
                redesenhaArvore();
            }
        }

        // Execução uma remoção na arvore e faz as chamadas dos metodos graficos
        // Redesenha o percurso com o node que substitui o valor removido
        public void remover(int valor)
        {
            No node = arvore.seleciona(Convert.ToInt32(valor));
            if (node == null)
            {
                richTextBox1.Text = "O valor " + valor + " não existe na Arvore";
            }
            else
            {
                if (node.esq == null && node.dir == null)
                {
                    iluminaNodeY(node.x, node.y, node.dado);
                    arvore.remove(valor);
                    apagaNode(node.x, node.y);
                }
                else
                {
                    iluminaNodeY(node.x, node.y, node.dado);
                    node = arvore.remove(valor);
                    iluminaNodeY(node.x, node.y, node.dado);

                    apagaNode(node.x, node.y);
                    node = arvore.seleciona(node.dado);
                    iluminaPercursoSimples(node);
                }
                richTextBox1.Text = valor + " Removido com sucesso.";
            }
        }

        // Limpa o panel, deixa a arvore null
        public void limpar()
        {
            Graphics formGraphics = panel1.CreateGraphics();
            Rectangle retangulo = new Rectangle(0, 0, panel1.Width, panel1.Height);
            formGraphics.FillRectangle(defaultColor, retangulo);
            richTextBox1.Clear();
            arvore = null;
        }

        #endregion Principais Ações

        #region Ações executadas por botões/radio buttons

        private void btnAction_Click(object sender, EventArgs e)
        {
            executarAcao();
            alert(@"Tecle ""Enter"" para confirmar uma ação.
Utilize as setas para cima e para baixo para alterar a ação executada");
        }

        private void txtboxAction_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                executarAcao();
            // Se o cliente pressionar Up ou Down e nenhum Radio esteja marcado,
            // o Inserir será marcado automáticamente.
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
                if(
                    rbtInserir.Checked == false &&
                    rbtSelecionar.Checked == false &&
                    rbtRemover.Checked == false &&
                    rbtMostrar.Checked == false)
                rbtInserir.Checked = true;
            else
            {
                if (e.KeyCode == Keys.Down)
                {
                    if (rbtInserir.Checked)
                        rbtRemover.Checked = true;
                    else
                        if (rbtRemover.Checked)
                            rbtSelecionar.Checked = true;
                        else
                            if (rbtSelecionar.Checked)
                                rbtMostrar.Checked = true;

                }
                if (e.KeyCode == Keys.Up)
                {
                    if (rbtRemover.Checked)
                        rbtInserir.Checked = true;
                    else
                        if (rbtSelecionar.Checked)
                            rbtRemover.Checked = true;
                        else
                            if (rbtMostrar.Checked)
                                rbtSelecionar.Checked = true;

                }
            }
        }

        private void rbtInserir_CheckedChanged(object sender, EventArgs e)
        {
            txtboxAction.Focus();
            richTextBox1.Text = @"Digite um número inteiro à ser inserido. 
A inserção funciona da seguinte forma:
Caso ainda não haja nenhum valor inserido, o 1º valor se tornará a ""Raiz"" da arvore.
Para cada inserção caso o valor inserido seja menor que um valor já existente na arvore, 
este se tornará ""Filho Esquerdo"" do valor existente.
Caso o valor inserido seja maior que um valor já existente na arvore,
este se tornará ""Filho Direito"" do valor existente.
Todo novo valor será inserido como ""Folha mais baixa"" da Arvore";
        }

        private void rbtSelecionar_CheckedChanged(object sender, EventArgs e)
        {
            txtboxAction.Focus();
            richTextBox1.Text = @"Digite um número inteiro à ser selecionado. A busca do Node é
realizada percorrendo a Arvore à partir de sua Raiz. Seguindo a mesma idéia da
inserção, caso o valor procurado for menor que o valor comparado na arvore, iremos
procurar à esquerda do mesmo. Caso o valor seja maior, iremos procurar à direita.";
        }

        private void rbtRemover_CheckedChanged(object sender, EventArgs e)
        {
            txtboxAction.Focus();
            richTextBox1.Text = @"Ao remover um valor, um Node-Filho irá lhe substituir, 
tendo à preferencia (caso exista) o último valor à direita de seu filho esquerdo. 
Preste atenção na substituição dos valores.";
        }

        private void rbtMostrar_CheckedChanged(object sender, EventArgs e)
        {
            txtboxAction.Focus();
            richTextBox1.Text = @"Digite um número inteiro à ser selecionado. 
Toda a Arvore/Sub-Arvore abaixo do Node selecionado será iluminada. 
Os nodes são selecionados através de uma recursão onde percorremos 
à arvore até seu ultimo filho esquerdo, verificamos o pai deste e seu filho direito. 
À partir do node direito (irmão do ultimo node esquerdo) percorremos a arvore novamente 
até seu último filho esquerdo e continuamos subindo pela arvore até que a mesma seja percorrida por completo.";

        }

        #endregion

        #region Inserção automática de diversos valores, para teste.

        private void btnTeste_Click(object sender, EventArgs e)
        {
            limpar();
            List<int> x = new List<int>();
            x.Add(200);
            x.Add(100);
            x.Add(300);
            x.Add(50);
            x.Add(150);
            x.Add(250);
            x.Add(350);
            x.Add(25);
            x.Add(75);
            x.Add(125);
            x.Add(175);
            x.Add(225);
            x.Add(275);
            x.Add(325);
            x.Add(375);
            x.Add(12);
            x.Add(37);
            x.Add(62);
            x.Add(87);
            x.Add(112);
            x.Add(137);
            x.Add(162);
            x.Add(187);
            x.Add(212);
            x.Add(237);
            x.Add(262);
            x.Add(287);
            x.Add(312);
            x.Add(337);
            x.Add(362);
            x.Add(387);

            for (int i = 0; i < x.Count; i++)
            {
                inserir(x[i], false);
            }
        }
        #endregion

        #region Limpando a tela e esvaziando a Arvore/BubbleSort

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpar();
            richTextBox1.Text = "Tudo zerado!";
        }

        #endregion

        #region Serialização
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.FileName = "ArvoreBinaria.Xml";
                saveFileDialog1.ShowDialog();
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro ocorrido ao salvar lista.\r\n" + erro.Message, "Amsterdan - Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            Serializa serializa = new Serializa();
            serializa.salvar(arvore.inorder(), saveFileDialog1.FileName);
            MessageBox.Show("Lista salva com sucesso!");
        }

        private void btnCarregar_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();

                Serializa serializa = new Serializa();
                limpar();
                List<int> listaNodes = serializa.carrega(openFileDialog1.FileName);
                for (int i = 0; i < listaNodes.Count; i++)
                {
                    inserir(listaNodes[i], false);
                }
                MessageBox.Show("Lista carregada com sucesso!", "Amsterdan - Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception erro)
            {
                alert("Erro ocorrido ao carregar lista.\r\n" + erro.Message);
            }
        }
        #endregion

    }
}
