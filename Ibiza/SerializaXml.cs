﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace Amsterdan
{
    class SerializaXml:ISerializa
    {
        public void salvar(List<int> listaInsercao, string caminho)
        {
            using (XmlWriter escreve = XmlWriter.Create(caminho))
            {
                escreve.WriteStartElement("Arvore_Binaria");
                for (int i = 0; i < listaInsercao.Count; i++)
                {
                    escreve.WriteElementString("Node", listaInsercao[i].ToString());    
                }
                escreve.WriteEndElement();
                escreve.Flush();
            }
        }
        public List<int> carrega(string caminho)
        {
            List<int> listaDeNodes = new List<int>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(caminho);
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Node");
            foreach (XmlNode node in nodeList)
            {
                listaDeNodes.Add(Convert.ToInt32(node.InnerText));
            }
            return listaDeNodes;
        }
    }
}
