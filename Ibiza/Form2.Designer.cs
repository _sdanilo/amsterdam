﻿namespace Amsterdan
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLimpar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtMostrar = new System.Windows.Forms.RadioButton();
            this.btnCarregar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnTeste = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.rbtSelecionar = new System.Windows.Forms.RadioButton();
            this.rbtRemover = new System.Windows.Forms.RadioButton();
            this.rbtInserir = new System.Windows.Forms.RadioButton();
            this.txtboxAction = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLimpar
            // 
            this.btnLimpar.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Location = new System.Drawing.Point(7, 221);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(98, 40);
            this.btnLimpar.TabIndex = 9;
            this.btnLimpar.Text = "Limpar Arvore";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLimpar);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.rbtMostrar);
            this.groupBox2.Controls.Add(this.btnCarregar);
            this.groupBox2.Controls.Add(this.btnSalvar);
            this.groupBox2.Controls.Add(this.btnTeste);
            this.groupBox2.Controls.Add(this.btnAction);
            this.groupBox2.Controls.Add(this.rbtSelecionar);
            this.groupBox2.Controls.Add(this.rbtRemover);
            this.groupBox2.Controls.Add(this.rbtInserir);
            this.groupBox2.Controls.Add(this.txtboxAction);
            this.groupBox2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(2, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 433);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ações";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Digite Aqui:";
            // 
            // rbtMostrar
            // 
            this.rbtMostrar.AutoSize = true;
            this.rbtMostrar.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtMostrar.Location = new System.Drawing.Point(9, 84);
            this.rbtMostrar.Name = "rbtMostrar";
            this.rbtMostrar.Size = new System.Drawing.Size(105, 19);
            this.rbtMostrar.TabIndex = 18;
            this.rbtMostrar.Text = "Mostrar Arvore";
            this.rbtMostrar.UseVisualStyleBackColor = true;
            this.rbtMostrar.CheckedChanged += new System.EventHandler(this.rbtMostrar_CheckedChanged);
            // 
            // btnCarregar
            // 
            this.btnCarregar.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregar.Location = new System.Drawing.Point(7, 387);
            this.btnCarregar.Name = "btnCarregar";
            this.btnCarregar.Size = new System.Drawing.Size(97, 40);
            this.btnCarregar.TabIndex = 17;
            this.btnCarregar.Text = "Carregar lista";
            this.btnCarregar.UseVisualStyleBackColor = true;
            this.btnCarregar.Click += new System.EventHandler(this.btnCarregar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Location = new System.Drawing.Point(7, 327);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(97, 54);
            this.btnSalvar.TabIndex = 16;
            this.btnSalvar.Text = "Salvar lista de Inserção";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnTeste
            // 
            this.btnTeste.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTeste.Location = new System.Drawing.Point(7, 267);
            this.btnTeste.Name = "btnTeste";
            this.btnTeste.Size = new System.Drawing.Size(97, 54);
            this.btnTeste.TabIndex = 15;
            this.btnTeste.Text = "Simulação Automática";
            this.btnTeste.UseVisualStyleBackColor = true;
            this.btnTeste.Click += new System.EventHandler(this.btnTeste_Click);
            // 
            // btnAction
            // 
            this.btnAction.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAction.Location = new System.Drawing.Point(7, 154);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(100, 40);
            this.btnAction.TabIndex = 13;
            this.btnAction.Tag = "";
            this.btnAction.Text = "Ok";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // rbtSelecionar
            // 
            this.rbtSelecionar.AutoSize = true;
            this.rbtSelecionar.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtSelecionar.Location = new System.Drawing.Point(9, 61);
            this.rbtSelecionar.Name = "rbtSelecionar";
            this.rbtSelecionar.Size = new System.Drawing.Size(81, 19);
            this.rbtSelecionar.TabIndex = 14;
            this.rbtSelecionar.Text = "Selecionar";
            this.rbtSelecionar.UseVisualStyleBackColor = true;
            this.rbtSelecionar.CheckedChanged += new System.EventHandler(this.rbtSelecionar_CheckedChanged);
            // 
            // rbtRemover
            // 
            this.rbtRemover.AutoSize = true;
            this.rbtRemover.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtRemover.Location = new System.Drawing.Point(9, 38);
            this.rbtRemover.Name = "rbtRemover";
            this.rbtRemover.Size = new System.Drawing.Size(69, 19);
            this.rbtRemover.TabIndex = 13;
            this.rbtRemover.Text = "Remover";
            this.rbtRemover.UseVisualStyleBackColor = true;
            this.rbtRemover.CheckedChanged += new System.EventHandler(this.rbtRemover_CheckedChanged);
            // 
            // rbtInserir
            // 
            this.rbtInserir.AutoSize = true;
            this.rbtInserir.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtInserir.Location = new System.Drawing.Point(9, 15);
            this.rbtInserir.Name = "rbtInserir";
            this.rbtInserir.Size = new System.Drawing.Size(62, 19);
            this.rbtInserir.TabIndex = 12;
            this.rbtInserir.Text = "Inserir";
            this.rbtInserir.UseVisualStyleBackColor = true;
            this.rbtInserir.CheckedChanged += new System.EventHandler(this.rbtInserir_CheckedChanged);
            // 
            // txtboxAction
            // 
            this.txtboxAction.Location = new System.Drawing.Point(8, 125);
            this.txtboxAction.Name = "txtboxAction";
            this.txtboxAction.Size = new System.Drawing.Size(100, 23);
            this.txtboxAction.TabIndex = 3;
            this.txtboxAction.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtboxAction.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtboxAction_KeyDown);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.richTextBox1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(123, 352);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(549, 90);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "ArvoreBinaria.Xml";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(123, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 337);
            this.panel1.TabIndex = 13;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 452);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.groupBox2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 490);
            this.MinimumSize = new System.Drawing.Size(700, 490);
            this.Name = "Form2";
            this.Text = "Projeto Amsterdam 1.0 : Algoritmo de Arvore Binária";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtboxAction;
        private System.Windows.Forms.RadioButton rbtSelecionar;
        private System.Windows.Forms.RadioButton rbtRemover;
        private System.Windows.Forms.RadioButton rbtInserir;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Button btnTeste;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCarregar;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbtMostrar;
        private System.Windows.Forms.Label label1;
    }
}