﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amsterdan
{
    public class No
    {
        public int dado = 0;
        public No esq = null;
        public No dir = null;
        public No pai = null;
        public bool inserido;

        //Valores padrão usados na Raiz da arvore
        public int y = 8; // |||||||||| heigth
        public int x = 549 / 2 - 10; // ---------- width

        public void definirValoresRaiz(int x, int y){
            this.x = x;
            this.y = y;
        }


        public No()
        {
            dado = 0;
            esq = null;
            dir = null;
            pai = null;
        }

        public No(int v)
        {
            dado = v;
            esq = null;
            dir = null;
            pai = null;
        }
    }
}
