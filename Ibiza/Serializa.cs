﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amsterdan
{
    class Serializa : ISerializa
    {
        #region ISerializa Members

        public void salvar(List<int> listaInsercao, string caminho)
        {
            SerializaXml serializa = new SerializaXml();
            try
            {
                serializa.salvar(listaInsercao, caminho);
            }
            catch (Exception erro)
            {
                throw new Exception("Falha ao salvar os dados", erro);
            } 
        }

        public List<int> carrega(string caminho)
        {
            SerializaXml serializa = new SerializaXml();
            List<int> listaDeNodes;
            try
            {
                listaDeNodes = serializa.carrega(caminho);
            }
            catch (Exception erro)
            {
                throw new Exception("Falha ao carregar os dados", erro);
            }
            return listaDeNodes;
        }

        #endregion
    }
}
